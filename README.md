# ZKAuth #
Back end part of library for the zero knowledge password proof that is proven hard against dictionary attacks.

### Tests ###
mvn clean test

### Front end ###
https://bitbucket.org/zk_capstone/frontend