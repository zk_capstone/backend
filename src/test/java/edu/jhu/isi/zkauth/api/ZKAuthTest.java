package edu.jhu.isi.zkauth.api;

import com.lambdaworks.codec.Base64;
import com.lambdaworks.crypto.SCryptUtil;
import edu.jhu.isi.zkauth.config.Config;
import edu.jhu.isi.zkauth.entities.Proof;
import edu.jhu.isi.zkauth.entities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MessageDigest.class, ZKAuth.class})
public class ZKAuthTest {
    @Test
    public void testLogin() throws Exception {
        final String password = "password";
        final String scryptHash = SCryptUtil.scrypt(password, 64, 1, 1);
        final String[] splittedHash = scryptHash.split("\\$");
        final String salt = splittedHash[2];
        final String hashedPassword = splittedHash[3];

        final User user = createUser(hashedPassword, password, salt);
        final Proof proof = createProof(hashedPassword, user);

        assertTrue(ZKAuth.login(user, proof));
    }

    private Proof createProof(final String hashedPassword, final User user) throws NoSuchAlgorithmException {
        final long timestamp = new Date().getTime();
        final BigInteger exponent = new BigInteger(256, new Random());
        final BigInteger commitment = Config.g.modPow(exponent, Config.p);
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(("" + Config.g + commitment + user.getStatement() + user.getUsername() + timestamp).getBytes());
        final BigInteger challenge = new BigInteger(1, digest.digest()).mod(Config.q);
        final BigInteger proofInt = exponent.subtract((new BigInteger("password".getBytes())
                .add(new BigInteger(Base64.decode(hashedPassword.toCharArray()))).mod(Config.q))
                .multiply(challenge)).mod(Config.q);

        return new Proof(proofInt, commitment, timestamp);
    }

    private User createUser(final String hashedPassword, final String password, final String salt) {
        final String username = "user";
        final BigInteger power1 = Config.g.modPow(new BigInteger(password.getBytes()), Config.p);
        final BigInteger power2 = Config.g.modPow(new BigInteger(Base64.decode(hashedPassword.toCharArray())), Config.p);
        final BigInteger statement = power1.multiply(power2).mod(Config.p);
        return new User(username, statement, salt);
    }

    @Test(expected = IllegalStateException.class)
    public void testException() throws Exception {
        PowerMockito.mockStatic(MessageDigest.class);
        PowerMockito.when(MessageDigest.getInstance(Mockito.anyString())).thenThrow(new NoSuchAlgorithmException());
        ZKAuth.login(new User(), new Proof(new BigInteger("1"), new BigInteger("1"), new Date().getTime()));
        PowerMockito.verifyStatic();
    }

    @Test
    public void testCanInstantiate() throws Exception {
        final ZKAuth zkAuth = new ZKAuth();
        final Config config = new Config();
        assertNotNull(zkAuth);
        assertNotNull(config);
    }

    @Test
    public void testLoginReplay() throws Exception {
        final String password = "password";
        final String scryptHash = SCryptUtil.scrypt(password, 64, 1, 1);
        final String[] splittedHash = scryptHash.split("\\$");
        final String salt = splittedHash[2];
        final String hashedPassword = splittedHash[3];

        final User user = createUser(hashedPassword, password, salt);
        final Proof proof = createProof(hashedPassword, user);
        proof.setTimestamp(new Date().getTime() - 10000);

        assertTrue(!ZKAuth.login(user, proof));
    }
}