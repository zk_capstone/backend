/*
 * Copyright (c) 2016 Gijs Van Laer, Rono Dasgupta, and Aditya Patil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.jhu.isi.zkauth.api;

import edu.jhu.isi.zkauth.config.Config;
import edu.jhu.isi.zkauth.entities.Proof;
import edu.jhu.isi.zkauth.entities.User;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class ZKAuth {
    public static boolean login(final User user, final Proof proof) {
        try {
            if (new Date().getTime() - proof.getTimestamp() > 10000) {
                return false;
            }
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(("" + Config.g + proof.getCommitment() + user.getStatement() + user.getUsername() + proof.getTimestamp()).getBytes());
            final BigInteger challenge = new BigInteger(1, digest.digest()).mod(Config.q);
            final BigInteger verification = Config.g.modPow(proof.getProof(), Config.p).multiply(user.getStatement().modPow(challenge, Config.p)).mod(Config.p);
            return proof.getCommitment().equals(verification);
        } catch (final NoSuchAlgorithmException e) {
            throw new IllegalStateException("There seems to be something wrong with this version of the library.", e);
        }
    }
}
