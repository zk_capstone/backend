/*
 * Copyright (c) 2016 Gijs Van Laer, Rono Dasgupta, and Aditya Patil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.jhu.isi.zkauth.entities;

import com.google.gson.Gson;

import java.math.BigInteger;

public class Proof {
    private BigInteger proof;
    private BigInteger commitment;
    private long timestamp;

    public Proof() {
    }

    public Proof(final BigInteger proof, final BigInteger commitment, final long timestamp) {
        this.proof = proof;
        this.commitment = commitment;
        this.timestamp = timestamp;
    }

    public static Proof fromJson(final String proof) {
        return new Gson().fromJson(proof, Proof.class);
    }

    public BigInteger getProof() {
        return proof;
    }

    public BigInteger getCommitment() {
        return commitment;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }
}
